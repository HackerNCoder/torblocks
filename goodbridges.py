#!/usr/bin/env python3

import re
import requests
import datetime
import argparse

parser = argparse.ArgumentParser(prog = "goodbridges", description="A program to find bridges with usage in a specific country")
parser.add_argument("-c", "--country", help="the two letter country code", required=True, metavar="cc")
args = parser.parse_args()

dt = (datetime.datetime.utcnow() - datetime.timedelta(hours=1)).strftime("%Y-%m-%d-%H")

url = "https://collector.torproject.org/recent/bridge-descriptors/extra-infos/{}-09-00-extra-infos".format(dt)
r = requests.get(url)
lines = r.text.splitlines()
i = 0
len_lines = len(lines)
regex = f"^bridge-ips.*({args.country}=[0-9][0-9]+)"
while i < len_lines:
    bridgeinfo = lines[i]
    while not "@type bridge-extra-info 1.3" in lines[i]:
        x = re.search(regex, lines[i])
        if x:
            bridgeinfo = bridgeinfo.replace("extra-info ", "")
            output = f"{bridgeinfo} {x.group(1)}"
            print(output)
        i += 1
        if len_lines <= i: break
    i += 1
