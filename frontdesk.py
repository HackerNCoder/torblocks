#! /usr/bin/env python3

import requests
import os
import re

countries = ["cn", "russia", "iran"]
frontdesk = ["237A3AA41A7F21003912B1B5DD996BF44B6256B7", "53A261C42A3C846CFDAD8DC65DE7C6C00DDD25C4", "CC1C4BDC809D22DC57E12A594545BC96F86401CE", "D9E712E593400635462172121B7DB90B07669F71", "E66A56969205AB856CF63165184A27C701D13820", "F42F0B2D45C9777ED22067E215AE572245C265EB", "FD96EFC317B7B3C3DBA25743945C9FF9496F63A3", "2CE8DEB3C10368637CE17452CF70D94AEB8857AD", "B02D184F901A2E997202343C2D52618C2DA838D4", "BB3ACC71001ED354F63019048BDA4A5CDAFB6A76", "347084323C43FD51B3D59DEA1AD1F5372A317589", "22E2530788E68B680CDCE861B65EC77389681BF3", "6291C82A2FCF606022219D40E2A03390A1655EAB"]

for country in countries:
    url = f"https://gitlab.torproject.org/tpo/anti-censorship/connectivity-measurement/bridgestatus/-/raw/main/recentResult_{country}"
    r = requests.get(url)
    lines = r.text.splitlines()
    i = 1
    for line in lines:
        splitline = line.split("\t")
        if splitline[0] not in frontdesk and not re.search("snowflake|email|https|moat|reserved|settings|telegram", splitline[0], re.I):
            filename = f"bridgestatus/{country}/{i}.csv"
            i+=1
        else:
            filename = f"bridgestatus/{country}/{splitline[0]}.csv"
        if os.path.exists(filename):
            lastline = open(filename).read().splitlines()[-1].split(",")[0]
        else:
            lastline = ""
        if splitline[2] != lastline:
            with open(filename, "a") as csvfile:
                if splitline[1] == "100":
                    csvfile.write(f"{splitline[2]},{splitline[1]},true,{splitline[5]}\n")
                elif int(splitline[1]) > 10:
                    csvfile.write(f"{splitline[2]},{splitline[1]},timeout,0\n")
                else:
                    csvfile.write(f"{splitline[2]},{splitline[1]},false,0\n")
