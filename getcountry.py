#! /usr/bin/env python3
import os
import argparse

parser = argparse.ArgumentParser(prog = "goodbridges", description="A program to find bridges with usage in a specific country")
parser.add_argument("-c", "--country", help="the two letter country code", required=True, metavar="cc")
args = parser.parse_args()

directory = os.listdir(f"bridgestatus/{args.country}")
for file in directory:
  with open(f"bridgestatus/{args.country}/{file}") as csv:
    print(file)
    for data in csv:
      print(f"\t{data.strip()}")
